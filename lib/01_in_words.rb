class Fixnum

  def in_words
    numbers = {
      1_000_000_000_000 => "trillion",
      1_000_000_000 => "billion",
      1_000_000 => "million",
      1_000 => "thousand",
      100 => "hundred",
      90 => "ninety",
      80 => "eighty",
      70 => "seventy",
      60 => "sixty",
      50 => "fifty",
      40 => "forty",
      30 => "thirty",
      20 => "twenty",
      19 => "nineteen",
      18 => "eighteen",
      17 => "seventeen",
      16 => "sixteen",
      15 => "fifteen",
      14 => "fourteen",
      13 => "thirteen",
      12 => "twelve",
      11 => "eleven",
      10 => "ten",
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one",
      0 => "zero"
    }
    numbers.each do |k, v|
      if self == 0
        return numbers[0]
      elsif self < 10 && (self / k) > 0
        return v.to_s
      elsif self < 100 && (self / k) > 0
        return v.to_s if (self % k) == 0
        return "#{v} " + (self % k).in_words
      elsif (self / k) > 0
        return (self / k).in_words + " #{v}" if (self % k) == 0
        return (self / k).in_words + " #{v} " + (self % k).in_words
      end
    end
  end

end
